#Python Learning Management System
##Created by Abhimanyu Deora

This is a Learning Management System that I made for NCTSA 2014 Open Source Software Development.

Required to run this web app - 

* Python 2.7
* Web server configured to run Python CGI scripts in the /cgi-bin folder
* MySQL Database
* [mysql.connector python library](https://dev.mysql.com/downloads/connector/python/)
