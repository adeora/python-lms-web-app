#!/usr/bin/python

import mysql.connector
import cgi
import re

data = cgi.FieldStorage()
connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

#sets up and executes mysql query to get the assignments
query = ("SELECT name, description, due_date FROM Assignments WHERE class_id=" + data["classID"].value)
cursor.execute(query)


print("Content-Type: text/html\n")
print("<h3>Assignments for " + data["className"].value +":</h3>")
for (name, description, due_date) in cursor:
	description = re.sub('[\n]', '<br />', description) #replaces \n's with <br />
	print('<h4 style="text-decoration: underline">{}</h4> <p style="padding-left: 20px;">{}</p> <p style="padding-left: 20px;">Due Date: {}</p>'.format(name, description, due_date))
cursor.close()
connection.close()
