#!/usr/bin/python

import mysql.connector
import cgi, cgitb
import re
cgitb.enable()

##################################
#setting up the database connection
##################################

data = cgi.FieldStorage()
connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

#####################################################
#making query getting student names, executing query
#####################################################

query = ("SELECT first_name, last_name FROM People WHERE id IN (SELECT people_id FROM Rosters WHERE class_id =" + data['classID'].value +")")
cursor.execute(query)

####################
#page header stuff
####################
print("Content-Type: text/html\n")
print("<h3>Your Class, " + data["className"].value +":</h3>")

############################################
#printing students registered for the class
############################################


print("<h4>Students Registered:</h4>")
for (first_name, last_name) in cursor:
	print('<p>{} {}</p>'.format(first_name, last_name))

#############################################
#creating form to create a class
############################################


query = ("SELECT id, first_name, last_name FROM People WHERE id IN (SELECT people_id FROM Rosters WHERE class_id =" + data['classID'].value +")")
cursor.execute(query) #get the data again with the id again

print("<h4>Add A Grade for this Class:</h4>")

#creates the dropdown with students names
print('Select the Student: <select id="student" class="selectpicker">')
for (id, first_name, last_name) in cursor:
	print('<option value="{}">{} {}</option>'.format(id, first_name, last_name))
print('</select><br />Select the Assignment: <select id="assignment" class="selectpicker">')

#query to get the id and name of assignments in that class
query = ("SELECT id, name FROM Assignments WHERE class_id=" + data['classID'].value)
cursor.execute(query)

#creates the dropdown with that classe's assignments
for (id, name) in cursor:
	print('<option value="{}">{}</option>'.format(id, name))
print("</select>")

#creates the input bar for the grade
print('<br />Grade in Percent: <input type="text" name="grade" id="grade"></input><br />')

#creates the link which when clicked sends the request to createGrade.py
print('<a href="#" onclick="createGrade()">Create Grade</a>')
print('<div id="gradeCreateError"></div>')



######################################################
#making query for getting assignments, executing query
######################################################
query = ("SELECT name, description, due_date FROM Assignments WHERE class_id=" + data['classID'].value)
cursor.execute(query)
######################################
#print assignments given to the class
######################################

print("<h4>Assignments Given:</h4>")
for (name, description, due_date) in cursor:
	description = re.sub('[\n]', '<br />', description)
	print('<p style="text-decoration: underline;">{}</p> <p style="padding-left: 20px;">{}</p><p style="padding-left: 20px;">Due Date: {}</p>'.format(name, description, due_date))

#####################################
#form for creating new assignments
#####################################

print('<h4>Create a New Assignment For This Class: </h4>')
print('<textarea rows="10" cols="70">Type the Assignment Information Here</textarea>')
print('<br><a href="#" onclick="createAssignment()">Create Assignment</a><br /><br />')
#close the connection
cursor.close()
connection.close()
