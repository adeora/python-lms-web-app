#!/usr/bin/python
import cgi
import cgitb
import sys
import json
import mysql.connector
import encodings #for encoding it to hex for the mysql query
cgitb.enable()


########################
#SETTING UP STUFF
########################
data = cgi.FieldStorage()
connection = mysql.connector.connect(user="root", password="GooglePlex314", host="localhost", database="TSA")
cursor = connection.cursor()



#########################
#PAGE HEADER INFO
#########################
sys.stdout.write("Content-Type: application/json")
sys.stdout.write("\n")
sys.stdout.write("\n")

#########################
#GETTING ALL THE DATA
#########################

name = data['name'].value
dueDate = data['dueDate'].value
classID = data['id'].value

#get the data, convert it to hex, prepend '0x'
stringPlaceHolder = data['description'].value
encodedString = stringPlaceHolder.encode("hex")
description = "0x" + encodedString

#########################
#GENERATING AND EXECUTING THE QUERY
########################

query = ("INSERT INTO `TSA`.`Assignments` (`id`, `name`, `description`, `due_date`, `class_id`) VALUES (NULL, '%s', %s, '%s', '%s');" %(name, description, dueDate, classID))

cursor.execute(query)
connection.commit()

############################
#CREATING AND SENDING RESULT
############################
result = {}
result['success'] = '<div class="alert alert-success">The Assignment was successfully created.</div>'

sys.stdout.write(json.dumps(result,indent=1))
sys.stdout.write("\n")
sys.stdout.close()
