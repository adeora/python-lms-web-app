#!/usr/bin/python

import cgi, cgitb 
cgitb.enable()  # for troubleshooting

#the cgi library gets vars from html
data = cgi.FieldStorage()
#this is the actual output
print("Content-Type: text/html\n")
print("<h3>Your Class, " + str(data['name'].value) + "</h3>")
print("<h4>Create a New Assignment for This Class</h4>")
print('<input class="form-control" type="text" name="assignmentName" id="assignmentName" placeholder="Assignment Name" />')
print('<textarea class="form-control" rows="10" cols="70" placeholder="Description of the Assignment"></textarea>')
print('<input id="assignmentDueDate" class="form-control" type="date" placeholder="Due Date of the Assignment. Please enter the date in the format yyyy-mm-dd" />')
print('<a href="#" onclick="createAssignment()" class="btn btn-primary">Create Assignment</a><br />')
print('<div id="error"></div>')

