#!/usr/bin/env python

import sys
import json
import mysql.connector
import cgi

#################
#GET FORM DATA
################

data = cgi.FieldStorage()

################
#SET UP JSON OBJECT
###############

sys.stdout.write("Content-Type: application/json")
sys.stdout.write("\n")
sys.stdout.write("\n")

#######################
#MYSQL DATABASE SETUP
#######################

connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

#############################
#CHECK TO SEE IF CLASS EXISTS
#############################

query = ("SELECT * FROM Classes WHERE name='" + data['className'].value + "'")
cursor.execute(query)
cursorData = cursor.fetchall()
cursor.close()


####################################
#CREATE CLASS IF CLASS DOESN'T EXIST
###################################
if len(cursorData) == 0: #if class exists
	query = ("INSERT INTO `TSA`.`Classes` (`id` ,`name` ,`teacher_id`) VALUES (NULL , '" + data['className'].value + "', '" + data['teacherID'].value + "');")
	cursor2 = connection.cursor()
	cursor2.execute(query)
	connection.commit()
	cursor2.close()
	created = True
else: #if class doesn't exist
	created = False #don't create the class


################################
#GET ID OF CREATED CLASS
###############################

if created == True:
	query = ("SELECT id FROM Classes WHERE name='" + data['className'].value + "'")
	cursor3 = connection.cursor()
	cursor3.execute(query)
	classId = cursor3.fetchone()

##################
#CREATE JSON RESULT
#################

result = {}
result['className'] = created
result['classID'] = classId[0]

sys.stdout.write(json.dumps(result,indent=1))
sys.stdout.write("\n")
sys.stdout.close()
