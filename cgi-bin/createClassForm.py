#!/usr/bin/python

import cgi
import cgitb
cgitb.enable()

print("Content-Type: text/html\n")
print("<h3>Create a Class:</h3>")
print('<input type="text" name="className" id="className" class="form-control" placeholder="Class Name"/>')
print('<a href="#" class="btn btn-primary" onclick="createClass()">Create Class</a>')
print('<div id="message"></div>')
