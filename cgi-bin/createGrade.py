#!/usr/bin/env python

import sys
import json
import cgi
import mysql.connector

#####################
#GET DATA FROM FORM
#####################

data = cgi.FieldStorage()

######################
#SET UP JSON OBJECT
######################

sys.stdout.write("Content-Type: application/json")
sys.stdout.write("\n")
sys.stdout.write("\n")

#######################
#MYSQL DATABASE STUFF
#######################

connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

query = ("INSERT INTO `TSA`.`Grades` (`id`, `score`, `student_id`, `assignment_id`) VALUES (NULL, '" + data['grade'].value + "', '" + data['student'].value + "', '" + data['assignment'].value + "');")

cursor.execute(query)
connection.commit()

#######################
#CREATE JSON RESULT
######################

result = {}
result['success'] = True



########################
#WRITE JSON
########################
sys.stdout.write(json.dumps(result,indent=1))
sys.stdout.write("\n")
sys.stdout.close()
