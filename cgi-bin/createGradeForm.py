#!/usr/bin/python

import mysql.connector
import cgi, cgitb
import re
cgitb.enable()

##################################
#setting up the database connection
##################################

data = cgi.FieldStorage()
connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

####################
#page header stuff
####################
print("Content-Type: text/html\n")
print("<h3>Your Class, " + data["className"].value +":</h3>")

############################
#ADDING A GRADE TO THIS CLASS
#############################

query = ("SELECT id, first_name, last_name FROM People WHERE id IN (SELECT people_id FROM Rosters WHERE class_id =" + data['classID'].value +")")
cursor.execute(query) #get the data again with the id again

print("<h4>Add A Grade for this Class:</h4>")

#creates the dropdown with students names
print('Select the Student: <select id="student" class="form-control">')
for (id, first_name, last_name) in cursor:
	print('<option value="{}">{} {}</option>'.format(id, first_name, last_name))
print('</select>Select the Assignment: <select id="assignment" class="form-control">')

#query to get the id and name of assignments in that class
query = ("SELECT id, name FROM Assignments WHERE class_id=" + data['classID'].value)
cursor.execute(query)

#creates the dropdown with that classe's assignments
for (id, name) in cursor:
	print('<option value="{}">{}</option>'.format(id, name))
print("</select>")

#creates the input bar for the grade
print('Grade in Percent: <input type="text" name="grade" id="grade" class="form-control"></input>')

#creates the link which when clicked sends the request to createGrade.py
print('<a href="#" onclick="createGrade()" class="btn btn-primary">Create Grade</a>')
print('<div id="gradeCreateError"></div>')



#close the connection
cursor.close()
connection.close()
