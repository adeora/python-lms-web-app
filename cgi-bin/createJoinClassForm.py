#!/usr/bin/python

import cgi
import cgitb
cgitb.enable()


print("Content-Type: text/html\n")
print("<h3>Join a Class</h3>")
print('<input type="text" class="form-control" name="classid" id="classid" placeholder="Class ID:" />')
print('<a href="#" class="btn btn-primary" onclick="joinClass()">Join the Class</a>')
print('<div id="message"></div>')
