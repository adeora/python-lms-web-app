#!/usr/bin/python
import mysql.connector
import cgi
import cgitb
import encodings
cgitb.enable()

data = cgi.FieldStorage()

connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()


###############################
#CREATE ANSWER ENTRY
##############################


aAnswer = data['aAnswer'].value.encode("hex")
bAnswer = data['bAnswer'].value.encode("hex")
cAnswer = data['cAnswer'].value.encode("hex")
dAnswer = data['dAnswer'].value.encode("hex")

makeAnswer = ("INSERT INTO `TSA`.`Answers` (a_answer, b_answer, c_answer, d_answer) VALUES ('%s', '%s', '%s', '%s')" %(aAnswer, bAnswer, cAnswer, dAnswer))

cursor.execute(makeAnswer)
connection.commit()
cursor.close()

#########################################################
#GET ID OF LAST CREATED ANSWER (THE ONE WE CREATED ABOVE)
#########################################################
cursor2 = connection.cursor()

getId = ("SELECT id FROM Answers ORDER BY id DESC LIMIT 1")
cursor2.execute(getId)
Idfetched = cursor2.fetchone()
answerID = Idfetched[0]

cursor2.close()

#####################################
#CREATE ENTRY IN QUESTION TABLE
#####################################

teacherID = data['teacherID'].value
topicID = data['topicID'].value
difficulty = data['difficulty'].value
correctAnswer = data['correctAnswer'].value
question = data['question'].value

cursor3 = connection.cursor()

query = ("INSERT INTO `TSA`.`Questions` (question, answer_id, correct_answer, difficulty, creator_id, topic_id) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')" %(question, answerID, correctAnswer, difficulty, teacherID, topicID))

cursor3.execute(query)
connection.commit()

connection.close()

print("Content-Type: text/html\n")
print('<div class="alert alert-success">The Question was created.</div>')
