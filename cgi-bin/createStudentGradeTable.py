#!/usr/bin/python

import cgi
import cgitb
import mysql.connector
cgitb.enable()

data = cgi.FieldStorage()
classID = data['classID'].value
className = data['className'].value

connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

query = ("SELECT first_name, last_name, name, score FROM Grades INNER JOIN Assignments ON Grades.assignment_id=Assignments.id INNER JOIN People ON Grades.student_id=People.id WHERE class_id=%s" %(classID))
cursor.execute(query)
pulledData = cursor.fetchall()
pulledData.sort()

print("Content-Type: text/html\n")
print("<h3>Your Class, %s</h3>" %(className))
print("<h4>Grades Given to This Class:</h4>")

print("""
<table class="table table-striped">
	<thead>
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Assignment Name</th>
			<th>Score</th>
		</tr>
	</thead>
	<tbody>
""")

for entry in pulledData:
	print("""
	<tr>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>""" %(entry[0], entry[1], entry[2], entry[3]))

print("""</tbody>
</table>""")
