#!/usr/bin/python

import cgi
import cgitb
import mysql.connector
cgitb.enable()

###WHAT THIS NEEDS TO DO
#  1) Take quiz code, which has the things below, seperate by underscores
#      a. Topic Id
#      b. # of Easy
#      c. # of Medium
#      d. # of Hard
#
#  2) Return a quiz w/ the easy, medium, and hard questions

#DATA SENT TO THINGY
#  1) className
#  2) quizCode

#DATA RETURNED
#  1) Quiz formatted in HTML

data = cgi.FieldStorage()
connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

className = data['className'].value
quizCode = data['quizCode'].value

splitCode = quizCode.split("_")
splitCode = [ int(x) for x in splitCode ]

#split code formatted like [topicId, #easy, #medium, #hard]
topicId = splitCode[0]
numEasy = splitCode[1]
numMed = splitCode[2]
numHard = splitCode[3]

print("Content-Type: text/html\n")

#get easy questions, w/ difficulty of 1

query = ("SELECT question, a_answer, b_answer, c_answer, d_answer FROM Questions INNER JOIN Answers ON Questions.answer_id = Answers.id WHERE topic_id=%s AND difficulty=1 ORDER BY RAND() LIMIT %s") %(topicId, numEasy)
cursor.execute(query)

easyQuestions = cursor.fetchall()

#get medium questions, w/difficulty of 2

query = ("SELECT question, a_answer, b_answer, c_answer, d_answer FROM Questions INNER JOIN Answers ON Questions.answer_id = Answers.id WHERE topic_id=%s AND difficulty=2 ORDER BY RAND() LIMIT %s") %(topicId, numMed)
cursor.execute(query)

medQuestions = cursor.fetchall()

#get hard questions, w/difficulty of 3

query = ("SELECT question, a_answer, b_answer, c_answer, d_answer FROM Questions INNER JOIN Answers ON Questions.answer_id = Answers.id WHERE topic_id=%s AND difficulty=3 ORDER BY RAND() LIMIT %s") %(topicId, numHard)
cursor.execute(query)

hardQuestions = cursor.fetchall()

allQuestions = easyQuestions + medQuestions + hardQuestions

print("<h3>Quiz for %s</h3>" %(className))

iterator = 1
for question in allQuestions:
	print("<h4>Question %s:" %(iterator))
	print("%s</h4>%s<br/>%s<br/>%s<br/>%s<br/><br/>" %(question[0], "A: " + question[1].decode("hex"), "B: " + question[2].decode("hex"), "C: " + question[3].decode("hex"), "D: " + question[4].decode("hex")))
	iterator += 1
