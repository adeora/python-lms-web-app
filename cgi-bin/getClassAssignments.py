#!/usr/bin/python

import mysql.connector
import cgi, cgitb
import re
cgitb.enable()

##################################
#setting up the database connection
##################################

data = cgi.FieldStorage()
connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

####################
#page header stuff
####################
print("Content-Type: text/html\n")
print("<h3>Your Class, " + data["className"].value +":</h3>")


######################################################
#making query for getting assignments, executing query
######################################################
query = ("SELECT name, description, due_date FROM Assignments WHERE class_id=" + data['classID'].value)
cursor.execute(query)
######################################
#print assignments given to the class
######################################

print("<h4>Assignments Given:</h4>")
for (name, description, due_date) in cursor:
	description = re.sub('[\n]', '<br />', description)
	print('<p style="text-decoration: underline;">{}</p> <p style="padding-left: 20px;">{}</p><p style="padding-left: 20px;">Due Date: {}</p>'.format(name, description, due_date))

#close the connection
cursor.close()
connection.close()
