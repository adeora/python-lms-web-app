#!/usr/bin/env python

import sys
import json
import cgi
import mysql.connector

#####################
#GET DATA FROM FORM
#####################

data = cgi.FieldStorage()

######################
#SET UP JSON OBJECT
######################

sys.stdout.write("Content-Type: application/json")
sys.stdout.write("\n")
sys.stdout.write("\n")

#######################
#MYSQL DATABASE STUFF
#######################

connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

query = ("SELECT password, id, isTeacher FROM People WHERE username='" + data['username'].value + "'")  #GET PASSWORD

cursor.execute(query)

databaseStorage = cursor.fetchone()

#######################
#CREATE JSON RESULT
######################

result = {}



########################
#LOGIN TEST OPERATION
########################
if databaseStorage == None:
	result['happened'] = False
	#result['userID'] = null
	#result['isTeacher'] = null
elif databaseStorage[0] == data['password'].value:
	result['happened'] = True


	#query = ("SELECT isTeacher FROM People where id=" + userID[0])
	#cursor.execute(query)
	#isTeacher = cursor.fetchone()

	result['userID'] = databaseStorage[1]
	result['isTeacher'] = databaseStorage[2]

else:
	result['happened'] = False
	#result['userID'] = null
	#result[isTeacher'] = null

########################
#WRITE JSON
########################
sys.stdout.write(json.dumps(result,indent=1))
sys.stdout.write("\n")
sys.stdout.close()
