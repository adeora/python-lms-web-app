#!/usr/bin/python

import cgi
import cgitb
import mysql.connector
cgitb.enable()

data = cgi.FieldStorage()

connection = mysql.connector.connect(user="root", password="GooglePlex314", host="localhost", database="TSA")
cursor = connection.cursor()

query = ("SELECT id, name FROM `TSA`.`Topics` WHERE class_id=%s"%(data['classID'].value))
cursor.execute(query)

fetched = cursor.fetchall()

print("Content-Type: text/html\n")

print("<h3>Your Class, %s</h3>" %(data['name'].value))
print("<h4>Create a New Question For this Class</h4>")
print('<textarea class="form-control" type="text" name="question" id="question" placeholder="Input the Question" />')
print('<input class="form-control" type="text" name="difficulty" id="difficulty" placeholder="Input the Difficulty (1-3)" />')
print('<input class="form-control" type="text" name="correctAnswer" id="correctAnswer" placeholder="Correct Answer: (A, B, C, or D)" />')
print('<textarea class="form-control" type="text" name="aAnswer" id="aAnswer" placeholder="The A Answer" />')
print('<textarea class="form-control" type="text" name="bAnswer" id="bAnswer" placeholder="The B Answer" />')
print('<textarea class="form-control" type="text" name="cAnswer" id="cAnswer" placeholder="The C Answer" />')
print('<textarea class="form-control" type="text" name="dAnswer" id="dAnswer" placeholder="The D Answer" />')
print('Select The Topic: <select id="topic" class="form-control">')
for i in fetched:
	print('<option value="%s">%s</option>' %(i[0], i[1]))
print("</select>")
print('<a href="#" onclick="createQuestion()" class="btn btn-primary">Create the Question</a>')
print('<div id="message"></div>')
