#!/usr/bin/python

import cgi
import cgitb
import mysql.connector
cgitb.enable()

#THIS MAKES:
# A form with these inputs
#  1) Topics in that class
#  2) Number of Easy
#  3) Number of Medium
#  4) Number of Hard

#Receives:
#  1) className
#  2) classId

data = cgi.FieldStorage()
className = data['className'].value
classId = data['classId'].value

connection = mysql.connector.connect(user='root', password='GooglePlex314', database='TSA', host='localhost')
cursor = connection.cursor()

query = ("SELECT id, name FROM Topics WHERE class_id=%s" %(classId))
cursor.execute(query)

print("Content-Type: text/html\n")

print("<h3>Your Class, %s" %(className))
print("<h4>Create a Quiz for this Class</h4>")
print('Select the Topic: <select id="topic" class="form-control">')

for (topic_id, name) in cursor:
	print('<option value="%s">%s</option>' %(topic_id, name))

print("</select>")

print('<input type="text" name="numEasy" id="numEasy" class="form-control" placeholder="Number of Easy Questions"></input>')
print('<input type="text" name="numMed" id="numMed" class="form-control" placeholder="Number of Medium Questions"></input>')
print('<input type="text" name="numHard" id="numHard" class="form-control" placeholder="Number of Hard Questions"></input>')
print('<a href="#" onclick="genQuizCode()" class="btn btn-primary">Create Quiz</a>')
print('<div id="quizMessage"></div>')

cursor.close()
connection.close()
