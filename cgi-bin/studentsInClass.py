#!/usr/bin/python

import mysql.connector
import cgi, cgitb
import re
cgitb.enable()

##################################
#setting up the database connection
##################################

data = cgi.FieldStorage()
connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

#####################################################
#making query getting student names, executing query
#####################################################

query = ("SELECT first_name, last_name FROM People WHERE id IN (SELECT people_id FROM Rosters WHERE class_id =" + data['classID'].value +")")
cursor.execute(query)

####################
#page header stuff
####################
print("Content-Type: text/html\n")
print("<h3>Your Class, " + data["className"].value +":</h3>")

############################################
#printing students registered for the class
############################################


print("<h4>Students Registered:</h4>")
for (first_name, last_name) in cursor:
	print('<p>{} {}</p>'.format(first_name, last_name))

cursor.close()
connection.close()
