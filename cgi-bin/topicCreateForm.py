#!/usr/bin/python
import cgi
import cgitb
cgitb.enable()

data = cgi.FieldStorage()


print("Content-Type: text/html\n")
print("<h3>Your Class, %s</h3>" %(data['name'].value))
print("<h4>Create a Topic for This Class</h4>")
print('<input type="text" name="topic_name" id="topic_name" placeholder="Name of the Topic" class="form-control"/>')
print('<a onclick="createTopic()" class="btn btn-primary" >Create Topic</a>')
print('<div id="topic-success"></div>')
