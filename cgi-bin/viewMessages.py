#!/usr/bin/python

import cgi
import cgitb
import encodings
import mysql.connector
cgitb.enable()

data = cgi.FieldStorage()
class_id = data['class_id'].value
className = data['className'].value

connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

query = ("SELECT message_text, time_sent FROM Messages WHERE class_id=%s" %(class_id))
cursor.execute(query)

messages = cursor.fetchall()
print("Content-Type: text/html\n")
print("<h3>Your Class, %s</h3>" %(className))
print("<h4>The Messages sent to this class:</h4>")

for message in messages:
	print('<br/><h5 style="border-bottom: 1px solid #ccc; padding-bottom: 5px;"><strong>Message sent at %s </strong></h5>' %(message[1]))
	print("<p>%s</p>" %(message[0].decode("hex")))
