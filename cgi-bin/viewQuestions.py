#!/usr/bin/python

import cgi
import cgitb
import mysql.connector
import encodings
cgitb.enable()

data = cgi.FieldStorage()

teacherID = data['teacherID'].value
classID = data['classId'].value

connection = mysql.connector.connect(user='root', password='GooglePlex314', host='localhost', database='TSA')
cursor = connection.cursor()

print("Content-Type: text/html\n")
print("<h3>Your Class, %s</h3>" %(data['className'].value))
print("<h4>Questions for this Class: </h4>")

query = ("SELECT question, answer_id, correct_answer, difficulty, creator_id, topic_id FROM Questions INNER JOIN Topics ON Questions.topic_id=Topics.id WHERE class_id=%s"%(classID))

cursor.execute(query)

questionData = cursor.fetchall()

for question in questionData:
	answer_id = question[1]
	topic_id = question[5]

	query = ("SELECT a_answer, b_answer, c_answer, d_answer FROM Answers WHERE id=%s" %(answer_id))
	cursor.execute(query)
	answerData = cursor.fetchone()

	query = ("SELECT name FROM Topics WHERE id=%s" %(topic_id))
	cursor.execute(query)
	topicData = cursor.fetchone()

	print("<p><strong>Question: %s</strong></p>" %(question[0]))
	print("<p>A: %s</p>" %(answerData[0].decode("hex")))
	print("<p>B: %s</p>" %(answerData[1].decode("hex")))
	print("<p>C: %s</p>" %(answerData[2].decode("hex")))
	print("<p>D: %s</p>" %(answerData[3].decode("hex")))
	print("<p>Correct Answer: %s</p>" %(question[2]))
	print("<p>Difficulty: %s</p>" %(question[3]))
	print("<p>Topic: %s</p>" %(topicData[0]))

