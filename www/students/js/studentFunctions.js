	//INDEX.HTML	

	//FUNCTION TO GET CLASSES STUDENTS ARE REGISTERED FOR
	function getClasses(){    
	    if (document.getElementById("mobile-nav") != null){
		document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    }
	    ID = myCookie;
	    $.ajax({
	        url: "/cgi-bin/student.py",
	        type: "POST",
	        data: {userID: ID},
	        success: function(response){
		    $("#content-container").html(response);
		    //console.log(response);
	        }
	    });
	}

	//GET ASSIGNMENTS WHEN THE FUNCTION IS CALLED
	function getAssignments(ID, name) {
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    $.ajax({
		url: "/cgi-bin/assignments.py",
		type: "POST",
		data: {classID: ID, className: name },
		success: function(response){
		    $("#content-container").html(response);
		    console.log(response);
		    }
		});
	}

	//CREATE FORM TO JOIN A CLASS
	function createJoinClassForm(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    $.ajax({
		url: "/cgi-bin/createJoinClassForm.py",
		type: "GET",
		success: function(response){
		    $("#content-container").html(response);
		}
	    });
	}

	//JOIN A CLASS
	function joinClass(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    classID = $("#classid").val();
	    $.ajax({
		url: "/cgi-bin/studentJoinClass.py",
		type: "POST",
		data: {studentID: myCookie, classID: classID},
		success: function(response){
		    $("#message").html(response);
		}
	    });
	}


//CLASS.HTML

//FUNCTION TO GET INFO FROM QUERY STRING
	function getParameterByName(name) {
	    if (document.getElementById("mobile-nav") != null){
		document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    }
    	    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    	    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	//GET ASSIGNMENTS WHEN THE FUNCTION IS CALLED
	function getAssignments() {
	    if (document.getElementById("mobile-nav") != null){
		document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    }
	ID = getParameterByName('id');
	name = getParameterByName('name');
	    $.ajax({
		url: "/cgi-bin/assignments.py",
		type: "POST",
		data: {classID: ID, className: name },
		success: function(response){
		    $("#content-container").html(response);
		    console.log(response);
		    }
		});
	}

	function getGradeTable() {
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    classID = getParameterByName('id');
	    className = getParameterByName('name');
	    studentID = myCookie;
	    $.ajax({
		url: "/cgi-bin/studentGetGrades.py",
		type: "POST",
		data: {classID: classID, studentID: studentID, className: className},
		success: function(response){
		    $("#content-container").html(response);
		}
	    });
	}

	function createQuizForm() {
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    className = getParameterByName('name');
	    toInsert = "<h3>Your Class, " + className + '</h3><h4>Take a Quiz for This Class</h4><input class="form-control" type="text" name="quizCode" id="quizCode" placeholder="Input the Quiz Code" /><a href="#" onClick="takeQuiz()" class="btn btn-primary">Take the Quiz</a>'
	    $("#content-container").html(toInsert);
	}

	function takeQuiz() {
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    className = getParameterByName('name');
	    quizCode = $("#quizCode").val();
	    $.ajax({
		url: "/cgi-bin/genQuiz.py",
		type: "POST",
		data: {className: className, quizCode: quizCode},
		success: function(response){
		    $("#content-container").html(response);
		}
	    });
	}


	function viewMessages(){
	    classId = getParameterByName("id");
	    className = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/viewMessages.py",
		data: {class_id: classId, className: className},
		type: "POST",
		success: function(response){
		    $("#content-container").html(response);
		}
	    });
	}		    
