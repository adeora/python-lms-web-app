


nv.addGraph(function() {
  var chart = nv.models.discreteBarChart()
      .x(function(d) { return d.label })
      .y(function(d) { return d.value })
      .staggerLabels(false)
      .tooltips(false)
      .showValues(true)

  d3.select('#chart svg')
      .datum(getChartData())
    .transition().duration(500)
      .call(chart);

  nv.utils.windowResize(chart.update);

  return chart;
});







function getChartData() {
 console.log("Hello World");

 return  [ 
    {
      key: "Cumulative Return",
      values: [
        { 
          "label" : "Assignment 1" ,
          "value" : 85
        } , 
        { 
          "label" : "Assignment 2" , 
          "value" : 89
        } , 
        { 
          "label" : "Assignment 3" , 
          "value" : 96
        } , 
        { 
          "label" : "Assignment 4" , 
          "value" : 94
        } , 
      ]
    }
  ]

}


