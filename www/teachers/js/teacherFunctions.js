//GENERAL STUFF

//function to close mobile menu on item click



//INDEX.HTML

//FUNCTION TO AJAX TO GET TEACHERS CLASSES
	function getTeachersClasses(){    
	    if (document.getElementById("mobile-nav") != null){
		document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    }
	    ID = myCookie
	    $.ajax({
	        url: "/cgi-bin/teachersClasses.py",
	        type: "POST",
	        data: {userID: ID},
	        success: function(response){
		    $("#content-container").html(response);
		    //console.log(response);
	        }
	    });
	}

	function getClassCreateForm(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    $.ajax({
		url: "/cgi-bin/createClassForm.py",
		type: "GET",
		success: function(response){
		    $("#content-container").html(response);
		}
	     });
	}

	function createClass(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    className = $("#className").val();
	    $.ajax({
		url: "/cgi-bin/createClass.py",
		type: "POST",
		data: {className: className, teacherID: myCookie},
		success: function(response){
		    $("#message").html('<div class="alert alert-success">Your class was created with an ID of: ' + response.classID + "</div>");
		}
	    });
	}

//CLASS.HTML

//FUNCTION TO AJAX IN THE STUDENTS REGISTERED	
	function getStudentsRegistered() {
	    if (document.getElementById("mobile-nav") != null){
		document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    }
	    classId = getParameterByName("id");
	    className = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/studentsInClass.py",
		type: "POST",
		data: {className: className, classID: classId},
		success: function(response){
		    $('#content-container').html(response);
		    }
		});
	}


    	//FUNCTION TO AJAX IN THE FORM TO CREATE A GRADE	
	function createGradeForm() {
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    classId = getParameterByName("id");
	    className = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/createGradeForm.py",
		type: "POST",
		data: {className: className, classID: classId},
		success: function(response){
		    $('#content-container').html(response);
		    }
		});
	}


    	//FUNCTION TO AJAX IN THE FORM TO DISPLAY THE ASSIGNMENTS	
	function getClassAssignments() {
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    classId = getParameterByName("id");
	    className = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/getClassAssignments.py",
		type: "POST",
		data: {className: className, classID: classId},
		success: function(response){
		    $('#content-container').html(response);
		    }
		});
	}


    	//FUNCTION TO AJAX IN THE FORM TO CREATE AN ASSIGNMENT	
	function createAssignmentForm() {
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    className = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/createAssignmentForm.py",
		type: "POST",
		data: {name: className},
		success: function(response){
		    $('#content-container').html(response);
		    }
		});
	}

	
	//FUNCTION TO SEND THE GRADE TO CREATE TO THE DB
	function createGrade(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
				
	    studentId = $('#student').val();
	    console.log(studentId);
	    assignmentId = $('#assignment').val();
	    console.log(assignmentId);
	    grade = $('#grade').val();
	    console.log(grade);
				
	    $.ajax({
		url: "/cgi-bin/createGrade.py",
		type: "POST",
		//datatype: "JSON",
		data: {student: studentId, assignment: assignmentId, grade: grade},
		success: function(response){
	    	    //$('#gradeSuccess').html("The grade was added to the system.");
		    console.log(response.success);
		    if (response.success == true) {
			$('#gradeCreateError').html('<div class="alert alert-success">The grade entry was created.</div>');
		    	}
		    else {
			$('#gradeCreateError').html('<div class="alert alert-danger">There was an error creating your class.</div>');
		    	}
		    },
		error: function( textStatus, errorThrown){
		    console.log("error with Ajax");
		    console.log("Error Code:");
		    console.log(textStatus['status']);
		    }
		});
			
	}

	function createAssignment(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    name = $('#assignmentName').val();
	    description = $('textarea').val();
	    dueDate = $('#assignmentDueDate').val();
	    classID = getParameterByName("id");
	    $.ajax({
		url: "/cgi-bin/createAssignment.py",
		type: "POST",
		data: {name: name, description: description, dueDate: dueDate, id: classID},
		success: function(response){
			$('#error').html(response.success);
			}
		});
	}

	function createTopicForm(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    name = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/topicCreateForm.py",
		type: "GET",
		data: {name: name},
		success: function(response){
		    $('#content-container').html(response);
		    }
		});
	}

	function createTopic(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    name = $('#topic_name').val();
	    id = getParameterByName("id");
	    $.ajax({
		url: "/cgi-bin/createTopic.py",
		type: "POST",
		data: {name: name, id: id},
		success: function(response){
		    $("#topic-success").html(response);
		    }
		});
	}

	function viewTopics(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    id = getParameterByName("id");
	    name = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/viewTopics.py",
		type: "GET",
		data: {id: id, name: name},
		success: function(response){
		    $('#content-container').html(response);
		    }
		});
	}
				
	function createQuestionForm(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    name = getParameterByName("name");
	    id = getParameterByName("id");
	    $.ajax({
		url: "/cgi-bin/questionCreateForm.py",
		type: "GET",
		data: {name: name, classID: id},
		success: function(response){
		    $('#content-container').html(response);
		    }
		});
	}

	function createQuestion(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    aAnswer = $("#aAnswer").val();
	    bAnswer = $("#bAnswer").val();
	    cAnswer = $("#cAnswer").val();
	    dAnswer = $("#dAnswer").val();
	    topicID = $("#topic").val();
	    difficulty = $("#difficulty").val();
	    correctAnswer = $("#correctAnswer").val();
	    question = $("#question").val();

	    console.log(aAnswer);
	    console.log(bAnswer);
	    console.log(cAnswer);
	    console.log(dAnswer);

	    $.ajax({
		url: "/cgi-bin/createQuestion.py",
		type: "GET",
		data: {
		    aAnswer: aAnswer,
		    bAnswer: bAnswer,
		    cAnswer: cAnswer,
		    dAnswer: dAnswer,
		    teacherID: myCookie,
		    topicID: topicID,
		    difficulty: difficulty,
		    correctAnswer: correctAnswer,
		    question: question
		    },
		success: function(response){
		    $('#message').html(response);
		    }
		});
	}

	function getQuestions(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    className = getParameterByName("name");
	    classId = getParameterByName("id");
	    $.ajax({
		url: "/cgi-bin/viewQuestions.py",
		type: "GET",
		data: {teacherID: myCookie, className: className, classId: classId},
		success: function(response){
		    $("#content-container").html(response);
		}
	    });
	}

	function getGradeTable(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    classID = getParameterByName("id");
	    className = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/teacherGradeTable.py",
		data: {classID: classID, className: className},
		type: "GET",
		success: function(response){
		    $("#content-container").html(response);
		}
	    });
	}

	function createQuizForm(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    classID = getParameterByName("id");
	    className = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/quizCreateForm.py",
		data: {classId: classID, className: className},
		type: "GET",
		success: function(response){
		    $("#content-container").html(response);
		}
	    });
	}

	function genQuizCode(){
	    document.getElementById("mobile-nav").className = "navbar-collapse collapse";
	    topicId = $("#topic").val();
	    numEasy = $("#numEasy").val();
	    numMed = $("#numMed").val();
	    numHard = $("#numHard").val();
	    quizCode = topicId + "_" + numEasy + "_" + numMed + "_" + numHard
	    $('#quizMessage').html("The quiz code is: " + quizCode);
	}

	function createMessageForm(){
	    className = getParameterByName("name");
	    toInsert = '<h3>Your Class, ' + className + '</h3> <h4>Send a Message to this class: </h4> <textarea id="message" class="form-control" placeholder="Type the message to send to the class here" rows="10" cols="70"></textarea> <a href="#" onclick="sendMessage()" class="btn btn-primary">Send Message</a> <div id="message-box"></div>'
	    $('#content-container').html(toInsert);
	}

	function sendMessage(){
	    classId = getParameterByName("id");
	    teacherId = myCookie;
	    message_text = $("#message").val();
	    $.ajax({
		url: "/cgi-bin/sendMessage.py",
		data: {message_text: message_text, class_id: classId, teacher_id: teacherId},
		type: "POST",
		success: function(response){
		    $("#message-box").html(response);
		}
	    });
	}
	
	function viewMessages(){
	    classId = getParameterByName("id");
	    className = getParameterByName("name");
	    $.ajax({
		url: "/cgi-bin/viewMessages.py",
		data: {class_id: classId, className: className},
		type: "POST",
		success: function(response){
		    $("#content-container").html(response);
		}
	    });
	}		    
